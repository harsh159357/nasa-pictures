# Nasa Pictures

### [Apk](https://drive.google.com/file/d/1C71R5UVLgLVwp_vVo1ees-1yqVRdtoBX/view?usp=sharing)

### [App in Action](https://drive.google.com/file/d/1pKOvu22W-1onlIQ_3RNMlbOJf5qNcqaG/view?usp=sharing)

## Overview of Assignment
* App Follows MVVM Architecture Design Pattern
* Dependency Injection implemented using Koin
* Room used for Local Database
* Images are loaded Lazily using Glide
* Bitbucket and Git are used for VCS.
* 100 % Code in Kotlin

## Assignment By
# [Harsh Sharma](http://harsh159357.github.io/)
Senior Android Developer Infosys
[StackOverFlow](http://bit.ly/stackharsh)
[LinkedIn](http://bit.ly/lnkdharsh)
[Github](http://bit.ly/githarsh)
