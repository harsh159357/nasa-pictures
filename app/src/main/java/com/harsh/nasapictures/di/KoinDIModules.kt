package com.harsh.nasapictures.di

import com.harsh.nasapictures.data.repository.NasaPicturesRepository
import com.harsh.nasapictures.data.room.NasaDatabase
import com.harsh.nasapictures.viewmodel.BookMarksViewModel
import com.harsh.nasapictures.viewmodel.NasaViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.module

val viewModelModule = module {
    factory {
        NasaViewModel(get())
    }
    factory {
        BookMarksViewModel(get())
    }
}
val repositoryModule = module {
    single {
        NasaDatabase.getDatabase(
            androidApplication(),
            CoroutineScope(SupervisorJob())
        )
    }
    factory { NasaPicturesRepository(get<NasaDatabase>().nasaPicturesDao()) }
}