package com.harsh.nasapictures.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.harsh.nasapictures.NasaPicturesApp
import com.harsh.nasapictures.R
import com.harsh.nasapictures.data.model.NasaItem
import com.harsh.nasapictures.databinding.ActivityNasaPicturesBinding
import com.harsh.nasapictures.ui.adapter.PicturesGridAdapter
import com.harsh.nasapictures.util.Message
import com.harsh.nasapictures.util.NasaUtil.Companion.POSITION
import com.harsh.nasapictures.util.showMessage
import com.harsh.nasapictures.viewmodel.NasaViewModel
import com.like.LikeButton
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class NasaPicturesActivity : AppCompatActivity(), PicturesGridAdapter.PictureListener {
    companion object {
        fun createIntent(context: Context): Intent =
            Intent(context, NasaPicturesActivity::class.java)
    }

    private val nasaViewModel by inject<NasaViewModel> { parametersOf(this) }

    private val binding: ActivityNasaPicturesBinding by lazy {
        DataBindingUtil.setContentView<ActivityNasaPicturesBinding>(
            this,
            R.layout.activity_nasa_pictures
        ).apply {
            lifecycleOwner = this@NasaPicturesActivity
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.apply {
            toolBar.title = getString(R.string.app_name)
            toolBar.setOnMenuItemClickListener {
                when (it.itemId) {
                    R.id.action_about_app -> {
                        binding.root.showMessage(Message("About App", false))
                        true
                    }

                    R.id.action_bookmarks -> {
                        startActivity(BookMarksActivity.createIntent(this@NasaPicturesActivity))
                        true
                    }

                    else -> {
                        // If we got here, the user's action was not recognized.
                        // Invoke the superclass to handle it.
                        super.onOptionsItemSelected(it)
                    }
                }
            }
            val gridAdapter = PicturesGridAdapter()
            gridAdapter.pictureListener = this@NasaPicturesActivity
            adapter = gridAdapter

            nasaPictures.layoutManager = GridLayoutManager(this@NasaPicturesActivity, 2)
            nasaViewModel.nasaItems.observe(this@NasaPicturesActivity, {
                it.let {
                    gridAdapter.updateNasaGallery(it)
                    if (it.isEmpty()) {
                        binding.nasaPictures.visibility = View.GONE
                        binding.llNoItems.visibility = View.VISIBLE
                    } else {
                        binding.nasaPictures.visibility = View.VISIBLE
                        binding.llNoItems.visibility = View.GONE
                    }
                }
            })
        }
    }

    override fun onResume() {
        super.onResume()
        if (NasaPicturesApp.instance.dataChanged) {
            nasaViewModel.reloadItems()
            NasaPicturesApp.instance.dataChanged = false
        }
    }

    override fun onPictureClick(nasaItem: NasaItem, position: Int) {
        startActivity(NasaPictureDetailActivity.createIntent(this).apply {
            putExtra(POSITION, position)
        })
    }

    override fun onLikeUnlike(
        likeUnlike: Boolean,
        position: Int,
        likeButton: LikeButton,
        nasaItem: NasaItem
    ) {
        binding.root.showMessage(
            if (likeUnlike) Message(
                "${nasaItem.title} BookMarked",
                false
            ) else Message("${nasaItem.title} Un BookMarked", true)
        )
        nasaViewModel.addRemoveBookMark(likeUnlike, nasaItem, position)
    }
}