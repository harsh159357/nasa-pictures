package com.harsh.nasapictures.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.harsh.nasapictures.NasaPicturesApp
import com.harsh.nasapictures.R
import com.harsh.nasapictures.databinding.ActivityPictureDetailsBinding
import com.harsh.nasapictures.ui.adapter.SectionsPagerAdapter
import com.harsh.nasapictures.util.NasaUtil.Companion.POSITION

class NasaPictureDetailActivity : AppCompatActivity() {
    companion object {
        fun createIntent(context: Context): Intent =
            Intent(context, NasaPictureDetailActivity::class.java)
    }

    lateinit var pictureDetailPagerAdapter: SectionsPagerAdapter

    private val binding: ActivityPictureDetailsBinding by lazy {
        DataBindingUtil.setContentView<ActivityPictureDetailsBinding>(
            this,
            R.layout.activity_picture_details
        ).apply {
            lifecycleOwner = this@NasaPictureDetailActivity
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.apply {
            pictureDetailPagerAdapter = SectionsPagerAdapter(supportFragmentManager)
            NasaPicturesApp.instance.getNasaItemsList().forEachIndexed { index, nasaItem ->
                pictureDetailPagerAdapter.addFragment(
                    PictureDetailsFragment.getInstance(nasaItem, index), nasaItem.title
                )
            }
            viewPager.adapter = pictureDetailPagerAdapter
            viewPager.currentItem = intent.extras?.getInt(POSITION) ?: 0
        }
    }
}