package com.harsh.nasapictures.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.harsh.nasapictures.data.model.NasaItem
import com.harsh.nasapictures.databinding.NasaPictureItemBinding
import com.like.LikeButton
import com.like.OnLikeListener

class PicturesGridAdapter() : RecyclerView.Adapter<PicturesGridAdapter.ItemViewHolder>() {
    interface PictureListener {
        fun onPictureClick(nasaItem: NasaItem, position: Int)
        fun onLikeUnlike(likeUnlike: Boolean, position: Int, likeButton: LikeButton, nasaItem: NasaItem)
    }

    var nasaItems: MutableList<NasaItem> = mutableListOf()
    var pictureListener: PictureListener? = null

    class ItemViewHolder(val binding: NasaPictureItemBinding) :
            RecyclerView.ViewHolder(binding.root)

    fun updateNasaGallery(items: List<NasaItem>) {
        nasaItems = items.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = NasaPictureItemBinding.inflate(layoutInflater, parent, false)
        return ItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        val currentItem = nasaItems[position]
        holder.binding.nasaItem = currentItem
        holder.binding.pictureCard.setOnClickListener {
            pictureListener?.onPictureClick(currentItem, position)
        }
        holder.binding.pictureBookmark.isLiked = currentItem.bookMarked
        holder.binding.pictureBookmark.setOnLikeListener(object : OnLikeListener {
            override fun liked(likeButton: LikeButton) {
                pictureListener?.onLikeUnlike(true, position, likeButton, currentItem)
                nasaItems[position].bookMarked = true
            }

            override fun unLiked(likeButton: LikeButton) {
                pictureListener?.onLikeUnlike(false, position, likeButton, currentItem)
                nasaItems[position].bookMarked = false
            }
        });
        holder.binding.executePendingBindings()
    }

    override fun getItemCount(): Int {
        return nasaItems.size
    }
}