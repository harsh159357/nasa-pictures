package com.harsh.nasapictures.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.GridLayoutManager
import com.harsh.nasapictures.NasaPicturesApp
import com.harsh.nasapictures.R
import com.harsh.nasapictures.data.model.NasaItem
import com.harsh.nasapictures.databinding.ActivityBookmarksBinding
import com.harsh.nasapictures.ui.adapter.PicturesGridAdapter
import com.harsh.nasapictures.util.Message
import com.harsh.nasapictures.util.NasaUtil.Companion.POSITION
import com.harsh.nasapictures.util.showMessage
import com.harsh.nasapictures.viewmodel.BookMarksViewModel
import com.like.LikeButton
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class BookMarksActivity : AppCompatActivity(), PicturesGridAdapter.PictureListener {
    companion object {
        fun createIntent(context: Context): Intent =
            Intent(context, BookMarksActivity::class.java)
    }

    private val bookMarksViewModel by inject<BookMarksViewModel> { parametersOf(this) }

    private val binding: ActivityBookmarksBinding by lazy {
        DataBindingUtil.setContentView<ActivityBookmarksBinding>(
            this,
            R.layout.activity_bookmarks
        ).apply {
            lifecycleOwner = this@BookMarksActivity
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.apply {
            toolBar.title = getString(R.string.bookmarks)
            setSupportActionBar(toolBar)
            supportActionBar?.let {
                it.setDisplayHomeAsUpEnabled(true)
                it.setDisplayShowHomeEnabled(true)
            }
            toolBar.setNavigationOnClickListener {
                finish()
            }
            val gridAdapter = PicturesGridAdapter()
            gridAdapter.pictureListener = this@BookMarksActivity
            adapter = gridAdapter

            nasaPictures.layoutManager = GridLayoutManager(this@BookMarksActivity, 2)
            bookMarksViewModel.bookMarks.observe(this@BookMarksActivity, {
                it.let {
                    gridAdapter.updateNasaGallery(it)
                    if (it.isEmpty()) {
                        binding.nasaPictures.visibility = View.GONE
                        binding.llNoItems.visibility = View.VISIBLE
                    } else {
                        binding.nasaPictures.visibility = View.VISIBLE
                        binding.llNoItems.visibility = View.GONE
                    }
                }
            })
        }
    }

    override fun onResume() {
        super.onResume()
        if (NasaPicturesApp.instance.dataChanged) {
            bookMarksViewModel.reloadItems()
            NasaPicturesApp.instance.dataChanged = true
        }
    }

    override fun onPictureClick(nasaItem: NasaItem, position: Int) {
        startActivity(NasaPictureDetailActivity.createIntent(this).apply {
            putExtra(POSITION, NasaPicturesApp.instance.getNasaItemsList().indexOf(nasaItem))
        })
    }

    override fun onLikeUnlike(
        likeUnlike: Boolean,
        position: Int,
        likeButton: LikeButton,
        nasaItem: NasaItem
    ) {
        NasaPicturesApp.instance.dataChanged = true
        binding.root.showMessage(
            if (likeUnlike) Message(
                "${nasaItem.title} BookMarked",
                false
            ) else Message("${nasaItem.title} Un BookMarked", true)
        )
        bookMarksViewModel.addRemoveBookMark(likeUnlike, nasaItem, position)
    }
}