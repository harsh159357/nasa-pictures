package com.harsh.nasapictures.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.harsh.nasapictures.NasaPicturesApp
import com.harsh.nasapictures.data.model.NasaItem
import com.harsh.nasapictures.databinding.NasaPictureDetailsItemBinding
import com.harsh.nasapictures.ui.adapter.bindUrl
import com.harsh.nasapictures.util.Message
import com.harsh.nasapictures.util.NasaUtil.Companion.NASA_ITEM
import com.harsh.nasapictures.util.NasaUtil.Companion.POSITION
import com.harsh.nasapictures.util.showMessage
import com.harsh.nasapictures.viewmodel.NasaViewModel
import com.like.LikeButton
import com.like.OnLikeListener
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class PictureDetailsFragment : Fragment() {

    companion object {
        fun getInstance(nasaItem: NasaItem, position: Int): PictureDetailsFragment {
            val pictureDetailsFragment = PictureDetailsFragment()
            pictureDetailsFragment.arguments = Bundle().apply {
                putParcelable(NASA_ITEM, nasaItem)
                putInt(POSITION, position)
            }
            return pictureDetailsFragment
        }
    }

    private val nasaViewModel by inject<NasaViewModel> { parametersOf(this) }

    private var binding: NasaPictureDetailsItemBinding? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = NasaPictureDetailsItemBinding.inflate(inflater, container, false)
        return binding!!.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.apply {
            arguments?.getParcelable<NasaItem>(NASA_ITEM)?.let { nasaItem ->
                detailTitle.text = nasaItem.title
                detailDate.text = nasaItem.date
                detailExplanation.text = nasaItem.explanation
                detailPicture.bindUrl(nasaItem.hdurl)
                detailCopyright.text = nasaItem.copyright
                detailPictureBookmark.isLiked = nasaItem.bookMarked
                detailPictureBookmark.setOnLikeListener(object : OnLikeListener {
                    override fun liked(likeButton: LikeButton) {
                        nasaItem.bookMarked = true
                        nasaViewModel.addRemoveBookMark(
                            true,
                            nasaItem,
                            arguments?.getInt(POSITION)!!
                        )
                        likeUnlikeBookMark(true, nasaItem)
                    }

                    override fun unLiked(likeButton: LikeButton) {
                        nasaItem.bookMarked = false
                        nasaViewModel.addRemoveBookMark(
                            false,
                            nasaItem,
                            arguments?.getInt(POSITION)!!
                        )
                        likeUnlikeBookMark(false, nasaItem)
                    }
                });
                if (nasaItem.copyright == null) {
                    detailCopyright.visibility = View.GONE
                    textCopyright.visibility = View.GONE
                }
            }
        }
    }

    private fun likeUnlikeBookMark(likeUnlike: Boolean, nasaItem: NasaItem) {
        NasaPicturesApp.instance.dataChanged = true
        binding!!.root.showMessage(
            if (likeUnlike) Message(
                "${nasaItem.title} BookMarked",
                false
            ) else Message("${nasaItem.title} Un BookMarked", true)
        )
    }
}
