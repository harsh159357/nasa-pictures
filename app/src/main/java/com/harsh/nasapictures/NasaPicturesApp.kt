package com.harsh.nasapictures

import android.app.Application
import com.harsh.nasapictures.data.model.NasaItem
import com.harsh.nasapictures.di.repositoryModule
import com.harsh.nasapictures.di.viewModelModule
import com.harsh.nasapictures.util.NasaUtil
import org.koin.android.ext.android.startKoin
import java.util.*

class NasaPicturesApp : Application() {

    companion object {
        lateinit var instance: NasaPicturesApp
            private set
    }

    private lateinit var nasaItems: ArrayList<NasaItem>

    var dataChanged: Boolean = false

    override fun onCreate() {
        super.onCreate()
        instance = this
        startKoin(this, listOf(viewModelModule, repositoryModule))
        initNasaItems()
    }

    fun initNasaItems() {
        nasaItems = NasaUtil
            .getNasaItems(
                NasaUtil
                    .rawJsonToString(
                        resources,
                        R.raw.nasa_items
                    )
            )
    }

    fun getNasaItemsList(): ArrayList<NasaItem> {
        return nasaItems
    }
}