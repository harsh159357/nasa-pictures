package com.harsh.nasapictures.data.repository

import androidx.annotation.WorkerThread
import com.harsh.nasapictures.data.model.NasaItem
import com.harsh.nasapictures.data.room.NasaDao

class NasaPicturesRepository(private val dao: NasaDao) {

    fun getNasaPictures() = dao.getNasaPictures()

    @WorkerThread
    suspend fun insert(cartItem: NasaItem) {
        dao.insert(cartItem)
    }

    @WorkerThread
    suspend fun remove(cartItem: NasaItem) {
        dao.remove(cartItem)
    }

    @WorkerThread
    suspend fun clear() {
        dao.clear()
    }
}