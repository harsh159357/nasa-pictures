package com.harsh.nasapictures.data.room

import androidx.room.*
import com.harsh.nasapictures.data.model.NasaItem

@Dao
interface NasaDao {

    @Query("SELECT * FROM nasa_items ORDER BY title ASC")
    fun getNasaPictures(): List<NasaItem>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(cartItem: NasaItem)

    @Delete
    suspend fun remove(cartItem: NasaItem)

    @Query("DELETE FROM nasa_items")
    suspend fun clear()
}