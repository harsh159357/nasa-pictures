package com.harsh.nasapictures.data.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.harsh.nasapictures.data.model.NasaItem
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch


@Database(entities = [NasaItem::class], version = 2, exportSchema = false)
abstract class NasaDatabase : RoomDatabase() {

    abstract fun nasaPicturesDao(): NasaDao

    private class NasaDatabaseCallback(
        private val context: Context,
        private val scope: CoroutineScope
    ) : Callback() {

        override fun onCreate(db: SupportSQLiteDatabase) {
            super.onCreate(db)
            INSTANCE?.let { database ->
                scope.launch {
                    val nasaImageDao = database.nasaPicturesDao()
                    nasaImageDao.clear()
/*
                    NasaPicturesApp.instance.getNasaItemsList().forEach {
                        nasaImageDao.insert(it)
                    }
*/
                }
            }
        }
    }

    companion object {
        @Volatile
        private var INSTANCE: NasaDatabase? = null

        fun getDatabase(context: Context, scope: CoroutineScope): NasaDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    NasaDatabase::class.java,
                    "nasa_database"
                )
                    .addCallback(NasaDatabaseCallback(context, scope))
                    .fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                // return instance
                instance
            }
        }
    }
}