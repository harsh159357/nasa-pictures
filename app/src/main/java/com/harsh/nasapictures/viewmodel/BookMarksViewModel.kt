package com.harsh.nasapictures.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.harsh.nasapictures.data.model.NasaItem
import com.harsh.nasapictures.data.repository.NasaPicturesRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BookMarksViewModel(var repository: NasaPicturesRepository) : ViewModel() {

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    var bookMarks: MutableLiveData<List<NasaItem>> = MutableLiveData()

    init {
        loadNasaItems()
    }

    private fun loadNasaItems() {
        viewModelScope.launch(Dispatchers.Main) {
            _loading.value = true
        }
        viewModelScope.launch(Dispatchers.IO) {
            val items = repository.getNasaPictures()
            viewModelScope.launch(Dispatchers.Main) {
                bookMarks.value = items
                _loading.value = false
            }
        }
    }

    fun addRemoveBookMark(likeUnLike: Boolean, nasaItem: NasaItem, position: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            if (likeUnLike) {
                repository.insert(nasaItem)
            } else {
                repository.remove(nasaItem)
            }
            loadNasaItems()
        }
    }

    fun reloadItems() {
        loadNasaItems()
    }
}