package com.harsh.nasapictures.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.harsh.nasapictures.NasaPicturesApp
import com.harsh.nasapictures.data.model.NasaItem
import com.harsh.nasapictures.data.repository.NasaPicturesRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class NasaViewModel(var repository: NasaPicturesRepository) : ViewModel() {

    private val _loading = MutableLiveData<Boolean>()
    val loading: LiveData<Boolean>
        get() = _loading

    var nasaItems: MutableLiveData<List<NasaItem>> = MutableLiveData()

    init {
        loadNasaItems()
    }

    private fun loadNasaItems() {
        _loading.value = true
        viewModelScope.launch(Dispatchers.IO) {
            NasaPicturesApp.instance.initNasaItems()
            val items = NasaPicturesApp.instance.getNasaItemsList()
            val dataBaseItems = repository.getNasaPictures()
            if (dataBaseItems.isNotEmpty()) {
                dataBaseItems.forEach {
                    for (item in items) {
                        if (item.date == it.date) {
                            item.bookMarked = it.bookMarked
                        }
                    }
                }
            }
            viewModelScope.launch(Dispatchers.Main) {
                nasaItems.value = items
                _loading.value = false
            }
        }
    }

    fun addRemoveBookMark(likeUnLike: Boolean, nasaItem: NasaItem, position: Int) {
        viewModelScope.launch(Dispatchers.IO) {
            if (likeUnLike) {
                repository.insert(nasaItem)
            } else {
                repository.remove(nasaItem)
            }
        }
    }

    fun reloadItems() {
        loadNasaItems()
    }
}