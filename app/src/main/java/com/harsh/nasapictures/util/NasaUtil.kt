package com.harsh.nasapictures.util

import android.content.res.Resources

import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.harsh.nasapictures.R
import com.harsh.nasapictures.data.model.NasaItem
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.StringWriter
import java.io.Writer
import java.security.SecureRandom
import java.util.*

class NasaUtil {
    companion object {
        const val SPLASH_DELAY: Long = 2000

        const val NASA_ITEM: String = "NASA_ITEM"

        const val POSITION: String = "POSITION"

        //Some sample splash animations
        val splashAnimation = intArrayOf(
            R.anim.fade_in,
            R.anim.zoom_in,
            R.anim.slide_down,
            R.anim.bounce_down,
            R.anim.rotate_clockwise,
            R.anim.rotate_anti_clockwise
        )

        fun rawJsonToString(resources: Resources, id: Int): String {
            val resourceReader = resources.openRawResource(id)
            val writer: Writer = StringWriter()
            try {
                val reader = BufferedReader(InputStreamReader(resourceReader, "UTF-8"))
                var line = reader.readLine()
                while (line != null) {
                    writer.write(line)
                    line = reader.readLine()
                }
            } catch (e: Exception) {
                Log.e(
                    NasaUtil::class.simpleName,
                    "Unhandled exception while using rawJsonToString",
                    e
                )
            } finally {
                try {
                    resourceReader.close()
                } catch (e: Exception) {
                    Log.e(
                        NasaUtil::class.simpleName,
                        "Unhandled exception while using rawJsonToString",
                        e
                    )
                }
            }
            return writer.toString()
        }

        fun getNasaItems(json: String): ArrayList<NasaItem> {
            return if (json != "") {
                Gson().fromJson(
                    json,
                    object : TypeToken<ArrayList<NasaItem?>?>() {}.type
                )
            } else {
                ArrayList()
            }
        }

        fun generateRandomInteger(min: Int, max: Int): Int {
            val rand = SecureRandom()
            rand.setSeed(Date().time)
            return rand.nextInt(max - min + 1) + min
        }

        fun getRandomSplashAnimation(): Int {
            return splashAnimation[generateRandomInteger(0, splashAnimation.size - 1)]
        }
    }
}