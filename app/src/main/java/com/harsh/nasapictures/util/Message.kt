package com.harsh.nasapictures.util

data class Message(val message: String, val caution: Boolean)