package com.harsh.nasapictures.util

import android.view.View
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.harsh.nasapictures.R

fun View.showMessage(message: Message) {
    val snackBar = Snackbar.make(this, message.message, Snackbar.LENGTH_LONG)
    if (message.caution)
        snackBar.setBackgroundTint(ContextCompat.getColor(this.context, R.color.colorAccent))
    else
        snackBar.setBackgroundTint(ContextCompat.getColor(this.context, R.color.colorPrimary))
    snackBar.show()
}

fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.GONE
}
